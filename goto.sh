#!/bin/bash

LOCAL="local"
PREPROD="preprod"
FEATURE="feature"

__showGotoUsage() {
	echo "
Usage: 
    ${0} goto [${LOCAL} | ${PREPROD} | ${FEATURE} <featureNumber>] [-b | --back-office | -c <companyNumber> | --company <companyNumber>] [-h | --help]

    local                           open local url (by default)
    preprod                         open preprod url
    feature <featureNumber>         open specific feature url (attempted a feature number)
    -b, --back-office               open targeted env back-office
    -c, --company <companyNumber>   open targeted env company management page
    -h, --help                      get this message
"
}
__manageGotoOptions() {

	while :; do
		case $1 in
			$PREPROD) 
				target=$PREPROD
			;;
			$FEATURE) 
				shift
				target=$1
			;;
			-b|--back-office) 
				showBackOffice=true
			;;
			-c|--company)
				showCompany=true
				shift
				companyNumber=$1
			;;
			-h|--help) 
				__showGotoUsage
				exit 
			;;
			*) break
		esac
		shift
	done
}

goto() {	
	target=$LOCAL
	showBackOffice=false
	showCompany=false
	
	__manageGotoOptions $@
	
	case $target in 
		$LOCAL)
			url="http://localhost:4200"
			if $showBackOffice; then url="http://localhost:4900/backoffice"; fi
			if $showCompany; then url="http://localhost:4900/backoffice/entreprise/get/${companyNumber}"; fi
		;;
		$PREPROD)
			url="https://agicap.az-preprod.agcp-tech.net/"
			# TODO : manage backoffice
		;;
		*)
			url="https://agicappayment-feat-${target}.dev.agicap.cloud/"
			if $showBackOffice; then url=$url+"/backoffice"; fi
			if $showCompany; then url=$url+"/backoffice/entreprise/get/${companyNumber}"; fi
		;;
	esac
	
	start ${url}
}