#!/bin/bash

rootPath=$(realpath "$(dirname "${0}")")
sourcePath=$(realpath "$rootPath/..")

source $rootPath/prepare.sh
source $rootPath/run.sh
source $rootPath/goto.sh
source $rootPath/branch.sh
source $rootPath/checkout.sh

__showUsage() {
	echo "
Usage: 
    ${0} [prepare | run | branch | goto] [-h | --help]

    prepare         pull, restore, install and execute migration to prepare the environment
    run             run environment
    branch          get all repositories branch statuses
    goto            open targeted environment
    -h, --help      get this message
"
}

case $1 in
	prepare)
		shift
		prepare $@
	;;
	run)
		shift 
		run $@
	;;
	branch)
		shift
		branch $@
	;;
	goto)
		shift
		goto $@
	;;
	-h|--help|*)
		__showUsage
	;;
esac
