#!/bin/bash

write() {	
	RED="\033[0;31m"
    GREEN="\033[0;32m"
    YELLOW="\033[1;33m"
	BLUE="\033[1;34m"
    DEFAULT="\033[0m"
	
	color=$DEFAULT;
	text="";
	
	while [ "$1" != "" ];
	do
	   case $1 in
	   -g  | --green )  
			color=$GREEN
			;;
	   -r  | --red )  
			color=$RED
			;;
	   -y  | --yellow )  
			color=$YELLOW
			;;
	   -b  | --blue )  
			color=$BLUE
			;;
		*)
			text=$1
			;;
		esac
		shift
	done
	
    printf "${color}${text} ${DEFAULT}\n"
}

pull() {
	previousPath=$PWD
	cd $1
	
	write --blue "-- pull"
	git remote prune origin
	git fetch
	git pull --autostash
	
	cd $previousPath
}

checkoutTo() {
	previousPath=$PWD
	cd $1
	
	write --blue "-- checkout to " $2
	git stash 
	git checkout $2
	
	cd $previousPath
}

getStatus() {
	previousPath=$PWD
	cd $1
	
	write $1
	write --green "branch: $(git branch --show-current)" 
	
	cd $previousPath
}

restore() {
	write --blue "-- restore"
	dotnet restore $1
}

install() {
	previousPath=$PWD
	cd $1
	
	write --blue "-- install"
	npm install
	
	cd $previousPath
}

migrate() {
	targetPath=$(dirname $1)
	previousPath=$PWD
	cd $targetPath
	
	write --blue "-- migrate"
	dotnet run --project $1
	
	cd $previousPath
}

## Options with or without args
# while [ $# -gt 0 ]
# do
    # unset OPTIND
    # unset OPTARG
    # while getopts as:c:  options
    # do
    # case $options in
            # a)  echo "option a  no optarg"
                    # ;;
            # s)  serveur="$OPTARG"
                    # echo "option s = $serveur"
                    # ;;
            # c)  cible="$OPTARG"
                    # echo "option c = $cible"
                    # ;;
        # esac
   # done
   # shift $((OPTIND-1))
   # ARGS="${ARGS} $1 "
   # shift
# done

## With optional arguments
# while :; do
    # case $1 in
        # -a|--flag1) flag1="SET"            
        # ;;
        # -b|--flag2) flag2="SET"            
        # ;;
        # -c|--optflag1) optflag1="SET"            
        # ;;
        # -d|--optflag2) optflag2="SET"            
        # ;;
        # -e|--optflag3) optflag3="SET"            
        # ;;
        # *) break
    # esac
    # shift
# done

# ARGS=""
# echo "options :"
# while [ $# -gt 0 ]
# do
    # unset OPTIND
    # unset OPTARG
    # while getopts as:c:  options
    # do
    # case $options in
            # a)  echo "option a  no optarg"
                    # ;;
            # s)  serveur="$OPTARG"
                    # echo "option s = $serveur"
                    # ;;
            # c)  cible="$OPTARG"
                    # echo "option c = $cible"
                    # ;;
        # esac
   # done
   # shift $((OPTIND-1))
   # ARGS="${ARGS} $1 "
   # shift
# done

# echo "ARGS : $ARGS"
# exit 1