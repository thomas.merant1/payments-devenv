#!/bin/bash

rootPath=$(dirname "${0}")
source $rootPath/functions.sh

write "--- docker" --yellow 
docker container start payment-db 
docker container start rabbitmq

exit 0