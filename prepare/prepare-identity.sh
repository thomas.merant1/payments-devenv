#!/bin/bash

rootPath=$(realpath "$(dirname "${0}")/..")
sourcePath=$(realpath "$rootPath/..")

source $rootPath/functions.sh
       
write --yellow "---- Identity"  


target=$sourcePath/identity
  
getStatus $target
pull $target
restore $target/Identity.sln
