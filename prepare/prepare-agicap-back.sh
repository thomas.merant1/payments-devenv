#!/bin/bash

rootPath=$(realpath "$(dirname "${0}")/..")
sourcePath=$(realpath "$rootPath/..")

source $rootPath/functions.sh

write --yellow "---- Agicap back" 

target=$sourcePath/agicapapp
  
getStatus $target
pull $target
restore $target/AgicapAppCore.sln 

migrate $target/AgicapAppCore.FM/AgicapAppCore.FM.csproj
