#!/bin/bash

rootPath=$(realpath "$(dirname "${0}")/..")
sourcePath=$(realpath "$rootPath/..")

source $rootPath/functions.sh

write --yellow "---- Transformation Matrix"     

target=$sourcePath/transformationmatrix
  
getStatus $target
pull $target
restore $target

migrate $target/TransformationMatrix.Infrastructure.FM/TransformationMatrix.Infrastructure.FM.csproj
