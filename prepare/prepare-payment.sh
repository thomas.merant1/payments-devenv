#!/bin/bash

rootPath=$(realpath "$(dirname "${0}")/..")
sourcePath=$(realpath "$rootPath/..")

source $rootPath/functions.sh
        
write --yellow "---- Payment"   

target=$sourcePath/agicap-payments
  
getStatus $target
pull $target
restore $target/Back/Payments.sln

write --blue "-- migrate"

previousPath=$PWD
cd $target/Back/Payments.Infrastructure.FM/

dotnet run --project Payments.Infrastructure.FM.csproj --launch-profile MigrateDatabase --no-restore 

write --blue "-- migrate integration"
dotnet run --project Payments.Infrastructure.FM.csproj --launch-profile MigrateIntegrationDatabase --no-restore --no-build

cd $previousPath
