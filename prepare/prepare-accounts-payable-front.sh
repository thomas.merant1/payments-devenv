#!/bin/bash

rootPath=$(realpath "$(dirname "${0}")/..")
sourcePath=$(realpath "$rootPath/..")

source $rootPath/functions.sh

write --yellow "---- Accounts payable front"   

target=$sourcePath/accounts-payable-front
  
getStatus $target
pull $target
install $target