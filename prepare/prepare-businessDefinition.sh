#!/bin/bash

rootPath=$(realpath "$(dirname "${0}")/..")
sourcePath=$(realpath "$rootPath/..")

source $rootPath/functions.sh
       
write --yellow "---- Business definition"  


target=$sourcePath/businessDefinition
  
getStatus $target
pull $target
restore $target/BusinessDefinition.sln
