#!/bin/bash

rootPath=$(realpath "$(dirname "${0}")/..")
sourcePath=$(realpath "$rootPath/..")

source $rootPath/functions.sh
         
write --yellow "---- Aggregator"  

target=$sourcePath/aggregator
  
getStatus $target
pull $target
restore $target/Agregateur.sln

migrate $target/Agregateur.FM/Agregateur.FM.csproj
