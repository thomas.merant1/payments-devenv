#!/bin/bash

rootPath=$(realpath "$(dirname "${0}")/..")
sourcePath=$(realpath "$rootPath/..")

source $rootPath/functions.sh

write --yellow "---- Agicap front"  

target=$sourcePath/agicap-app-front
  
getStatus $target
pull $target
install $target