#!/bin/bash

rootPath=$(realpath "$(dirname "${0}")/..")
sourcePath=$(realpath "$rootPath/..")

source $rootPath/functions.sh

write --yellow "---- permissions"  

target=$sourcePath/agicap-permissions
  
getStatus $target
pull $target
restore $target

migrate $target/UserPermissions.Infrastructure.FM/UserPermissions.Infrastructure.FM.csproj
