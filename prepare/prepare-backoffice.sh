#!/bin/bash

rootPath=$(realpath "$(dirname "${0}")/..")
sourcePath=$(realpath "$rootPath/..")

source $rootPath/functions.sh

write --yellow "---- Back Office"       

target=$sourcePath/agicap-backoffice-front
  
getStatus $target
pull $target
install $target
