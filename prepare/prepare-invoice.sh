#!/bin/bash

rootPath=$(realpath "$(dirname "${0}")/..")
sourcePath=$(realpath "$rootPath/..")

source $rootPath/functions.sh
        
write --yellow "---- Invoice"   

target=$sourcePath/accounts-payable-invoices-management
  
getStatus $target
pull $target
restore $target/InvoicesManagement.sln

write --blue "-- migrate"

previousPath=$PWD
cd $target/InvoicesManagement.Infrastructure.FM/

dotnet run --project InvoicesManagement.Infrastructure.FM.csproj --launch-profile MigrateDatabase --no-restore 

write --blue "-- migrate integration"
dotnet run --project InvoicesManagement.Infrastructure.FM.csproj --launch-profile MigrateIntegrationDatabase --no-restore --no-build

cd $previousPath
