#!/bin/bash

rootPath=$(realpath "$(dirname "${0}")")
sourcePath=$(realpath "$rootPath/..")

source $rootPath/functions.sh

__showCheckoutUsage() {
	echo "
Usage: 
    ${0} checkout [master | preprod | develop] [-h | --help]

    -h, --help          Get this message
"
}

checkout() {
	while :; do
		case $1 in
			master) 
				checkoutTo $sourcePath/identity master
				checkoutTo $sourcePath/aggregator master
				checkoutTo $sourcePath/agicap-permissions main
				checkoutTo $sourcePath/transformationmatrix master
				
				checkoutTo $sourcePath/agicapapp master
				checkoutTo $sourcePath/agicap-app-front main
				checkoutTo $sourcePath/agicap-backoffice-front main
			;;
			preprod) 
				checkoutTo $sourcePath/identity preprod
				checkoutTo $sourcePath/aggregator preprod
				checkoutTo $sourcePath/agicap-permissions preprod
				checkoutTo $sourcePath/transformationmatrix preprod
				
				checkoutTo $sourcePath/agicapapp preprod
				checkoutTo $sourcePath/agicap-app-front preprod
				checkoutTo $sourcePath/agicap-backoffice-front preprod
			;;
			develop) 
				checkoutTo $sourcePath/identity develop
				checkoutTo $sourcePath/aggregator develop
				checkoutTo $sourcePath/agicap-permissions develop
				checkoutTo $sourcePath/transformationmatrix develop
				
				checkoutTo $sourcePath/agicapapp develop
				checkoutTo $sourcePath/agicap-app-front develop
				checkoutTo $sourcePath/agicap-backoffice-front develop
			;;
			-h|--help) 
			__showStatusUsage
			exit 
			;;
			*) break
		esac
		shift
	done
}