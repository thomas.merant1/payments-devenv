#!/bin/bash

rootPath=$(realpath "$(dirname "${0}")")
prepareScriptsPath=$rootPath/prepare
sourcePath=$(realpath "$rootPath/..")

runAgicap=false
paymentOnly=false

__showPrepareUsage() {
	echo "
Usage: 
    ${0} prepare [all | [-a | --agicap] [-p | --payment] [-i | --invoice]] [-r | --auto-run] [-h | --help]

    all                full prepare environment (pull, restore, migrate, install)
    -a, --agicap       target action into agicap environment
    -p, --payment      target action into payment environment
    -i, --invoice      target action into invoice environment
    -r, --auto-run     launch environment after preparation
    -h, --help         Get this message
"
}

__managePrepareOptions() {
	while :; do
		case $1 in
			all)
				agicap=true
				payment=true
				invoice=true
			;;
			-a|--agicap) agicap=true
			;;
			-p|--payment) payment=true
			;;
			-i|--invoice) invoice=true
			;;
			-r|--auto-run) autorun=true         
			;;
			-h|--help) 
				__showPrepareUsage
				exit 
			;;
			*) break
		esac
		shift
	done
}

prepare() {	
	agicap=false
	payment=false
	invoice=false
	autorun=false
	
	__managePrepareOptions $@
	args=("$@")
	
	sh $rootPath/run-docker-containers.sh
	
	if [[ $agicap = true ]]; then
		sh $prepareScriptsPath/prepare-businessDefinition.sh
		sh $prepareScriptsPath/prepare-permissions.sh
		sh $prepareScriptsPath/prepare-aggregator.sh
		sh $prepareScriptsPath/prepare-transformationmatrix.sh
		sh $prepareScriptsPath/prepare-backoffice.sh
		sh $prepareScriptsPath/prepare-agicap-back.sh
		sh $prepareScriptsPath/prepare-agicap-front.sh
	fi
	
	if [[ $payment = true ]]; then
		sh $prepareScriptsPath/prepare-payment.sh
	fi
	
	if [[ $invoice = true ]]; then
		sh $prepareScriptsPath/prepare-invoice.sh
	fi

	if [[ $payment = true ]] || [[ $invoice = true ]]; then
		sh $prepareScriptsPath/prepare-accounts-payable-front.sh
	fi

	if [[ $autorun = true ]]; then 
		run "${args[@]}"
	fi
}