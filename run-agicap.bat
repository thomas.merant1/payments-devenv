:: Dependencies
START "Identity" dotnet run --project identity/Identity.WebApi
START "Permissions" dotnet run --project agicap-permissions/UserPermissions.Web
START "Aggregator" dotnet run --project aggregator/Agregateur.Web
START "TransformationMatrix" dotnet run --project transformationmatrix/TransformationMatrix.Web

:: Agicap
START "Agicap - Back" dotnet run --project agicapapp/AgicapAppCore.Web/AgicapAppCore.Web.csproj
START "Agicap - FrontOffice" npm run start:local --prefix agicap-app-front
START "Agicap - Backoffice" npm run start:local --prefix agicap-backoffice-front

:: Payment
:: START "Payment - Back" dotnet run --project agicap-payments/Back/Payments.Web/Payments.Web.csproj
:: START "Payment - Front" npm run start:local:fr --prefix agicap-payments/Front