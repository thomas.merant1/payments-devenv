#!/bin/bash

rootPath=$(realpath "$(dirname "${0}")")
sourcePath=$(realpath "$rootPath/..")

source $rootPath/functions.sh

__showBranchUsage() {
	echo "
Usage: 
    ${0} branch [-p | --payment-only | -a | --agicap-only | -i | --invoice-only] [-h | --help]

	-a, --agicap-only   get branch status only for agicap scope
    -p, --payment-only  get branch status only for payment scope
	-i, --invoice-only 	get branch status only for invoice scope
    -h, --help          Get this message
"
}

__manageBranchOptions() {
	while :; do
		case $1 in
			-a|--agicap-only) 
				payment=false
				invoice=false
			;;
			-p|--payment-only) 
				agicap=false
				invoice=false
			;;
			-i|--invoic-only) 
				agicap=false
				payment=false
			;;
			-h|--help) 
			__showStatusUsage
			exit 
			;;
			*) break
		esac
		shift
	done
}

branch() {
	agicap=true
	payment=true
	invoice=true
	
	__manageBranchOptions $@
	
	if [[ $agicap = true ]]; then
		getStatus $sourcePath/businessDefinition
		getStatus $sourcePath/aggregator
		getStatus $sourcePath/agicap-permissions
		getStatus $sourcePath/transformationmatrix
		getStatus $sourcePath/agicapapp
		getStatus $sourcePath/agicap-app-front
		getStatus $sourcePath/agicap-backoffice-front
	fi
	
	if [[ $payment = true ]] || [[ $invoice = true ]]; then
		getStatus $sourcePath/accounts-payable-front
	fi
	
	if [[ $payment = true ]]; then
		getStatus $sourcePath/agicap-payments
	fi
	
	if [[ $invoice = true ]]; then
		getStatus $sourcePath/accounts-payable-invoices-management
	fi
}