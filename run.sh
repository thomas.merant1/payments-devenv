#!/bin/bash

rootPath=$(realpath "$(dirname "${0}")")
runScriptsPath=$rootPath/run
sourcePath=$(realpath "$rootPath/..")

runAgicap=false
paymentOnly=false

__showRunUsage() {
	echo "
Usage: 
    ${0} run [all | [-a | --agicap] [-p | --payment] [-i | --invoice]] [-h | --help]

    all                full run environment
    -a, --agicap       target action into agicap environment
    -p, --payment      target action into payment environment
    -i, --invoice      target action into invoice environment
    -h, --help         Get this message
"
}

__manageRunOptions() {
	while :; do
		case $1 in
			all)
				agicap=true
				payment=true
				invoice=true
			;;
			-a|--agicap) agicap=true
			;;
			-p|--payment) payment=true
			;;
			-i|--invoice) invoice=true
			;;
			-r|--auto-run) autorun=true         
			;;
			-h|--help) 
				__showRunUsage
				exit 
			;;
			*) break
		esac
		shift
	done
}

run() {	
	agicap=false
	payment=false
	invoice=false
	autorun=false
	
	__manageRunOptions $@
	
	sh $rootPath/run-docker-containers.sh
	
	previousPath=$PWD
	cd $sourcePath
	
	if [[ $agicap = true ]]; then
		$runScriptsPath/run-businessDefinition.bat
		$runScriptsPath/run-permissions.bat
		$runScriptsPath/run-aggregator.bat
		$runScriptsPath/run-transformationmatrix.bat
		$runScriptsPath/run-agicap-back.bat
		$runScriptsPath/run-agicap-front.bat
		$runScriptsPath/run-backoffice.bat
	fi
	
	
	if [[ $payment = true ]]; then
		$runScriptsPath/run-payment.bat
	fi
	
	if [[ $invoice = true ]]; then
		$runScriptsPath/run-invoice.bat
	fi

	if [[ $payment = true ]] || [[ $invoice = true ]]; then
		$runScriptsPath/run-accounts-payable-front.bat
	fi
	
	cd $previousPath
}