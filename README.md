# payments-devenv

Just a little tool tu faclilitate uses of payments local environment


## installation
Please save this repository in the same path that you saved all of agicap repositories.
You can save the path of payments-devenv repository into PATH variable environment in order to facilitate his usage.

## usages
```
Usage: 
    devenv.sh [prepare | run | branch | goto] [-h | --help]

    prepare         pull, restore, install and execute migration to prepare the environment
    run             run environment
    branch          get all repositories branch statuses
    goto            open targeted environment
    -h, --help      get this message
```

### prepare your local environment
```
Usage: 
    devenv.sh prepare [all | [-a | --agicap] [-p | --payment] [-i | --invoice]] [-r | --auto-run] [-h | --help]

    all                full prepare environment (pull, restore, migrate, install)
    -a, --agicap       target action into agicap environment
    -p, --payment      target action into payment environment
    -i, --invoice      target action into invoice environment
    -r, --auto-run     launch environment after preparation
    -h, --help         Get this message
```

### run your local environment
```
Usage: 
    devenv.sh run [all | [-a | --agicap] [-p | --payment] [-i | --invoice]] [-h | --help]

    all                full run environment
    -a, --agicap       target action into agicap environment
    -p, --payment      target action into payment environment
    -i, --invoice      target action into invoice environment
    -h, --help         Get this message
```

### open the agicap web page (feature, local, preprod, ...)
```
Usage: 
    devenv.sh goto [${LOCAL} | ${PREPROD} | ${FEATURE} <featureNumber>] [-b | --back-office | -c <companyNumber> | --company <companyNumber>] [-h | --help]

    local                           open local url (by default)
    preprod                         open preprod url
    feature <featureNumber>         open specific feature url (attempted a feature number)
    -b, --back-office               open targeted env back-office
    -c, --company <companyNumber>   open targeted env company management page
    -h, --help                      get this message
```

### get git branch info for all of repositories
```
Usage: 
    devenv.sh branch
```
